/**
 * Muestra las opciones de pantalla del juego.
 * @author Jose Ramon Martinez
 *
 */
public interface IScreen {
	/**
	 * Obtiene el ancho de pantalla.
	 * @return devuelve al ancho.
	 */

	public int getWidth();
	/**
	 * Configura el ancho.
	 * @param width
	 */
	public void setWidth(int width);
	
	public int getHeight();
	/**
	 * Configura la altura.
	 * @param height
	 */
	public void setHeight(int height);
	/**
	 * Crea un comando
	 * @return Devuelve true si se crea correctamente.
	 */
	public Boolean create();
	/**
	 * Inicia el juego.
	 */
	public void start();
	/**
	 * Para el juego.
	 * @param force
	 */
	public void stop(Boolean force);
	/**
	 * Cierra el juego.
	 * @param force
	 */
	public void close(Boolean force);
	/**
	 * Lee los comandos.
	 * @return Devuelve true si lee correctamente el comando.
	 */
	public ICommand readCommand();
	/**
	 * Ejecuta el comando que se ha introducido.
	 * @param command
	 * @return Devuelve true si se ha creado correctamente el comando.
	 */
	public Boolean executeCommand(ICommand command);
}
