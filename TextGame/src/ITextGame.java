/**
 * Se trata de una interfaz que te da diferentes opciones en el juego.
 * @author Jose Ramon Martinez Martinez
 *
 */
public interface ITextGame {
	/**
	 * Inicia el juego.
	 */
	public void start();
	/**
	 * Para el juego.
	 * @param force
	 */
	public void stop(Boolean force);
	/**
	 * Hace pausa en el juego.
	 * @param force
	 */
	public void pause(Boolean force);
	/**
	 * Hace una captura de pantalla del juego.
	 * @return Devuelve True si se ha hecho la captura de pantalla.
	 */
	public IScreen getScreen();
}
