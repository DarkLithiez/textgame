/**
 * Muestra las opciones de comandos.
 * @author alu53384053h
 *
 */
public interface ICommand {
	/**
	 * Crea el comando que nosotros introduzcamos.
	 * @param string
	 * @return Devuelve true si el comando es correcto.
	 */
	public Boolean create(String string);
	/**
	 * Corre el comando.
	 * @return Devuelve true si el comando se crea correctamente.
	 */
	public Boolean run();
}
